//performs both sign extend and zero extend depending upon the control signal

module data_ext (
	input [2:0] dataExtControl,
	input [15:0] dataExtIn,
	input shiftEn,
	output reg [15:0] dataExtOut
	);


	
    always@(*) begin
	    case (dataExtControl)
		    3'b001:	//5 bit sign extend
			    dataExtOut[15:0] = {{11{dataExtIn[4]}},dataExtIn[4:0]};
		    3'b010:	//6 bit	sign extend
			    dataExtOut = {{10{dataExtIn[5]}},dataExtIn[5:0]};
		    3'b011: //8 bit	sign extend
			    dataExtOut = {{8{dataExtIn[7]}},dataExtIn[7:0]};
		    3'b100:	//9 bit	sign extend
			    dataExtOut = {{7{dataExtIn[8]}},dataExtIn[8:0]};
		    3'b101: //11 bit sign extend	
			    dataExtOut = {{5{dataExtIn[10]}},dataExtIn[10:0]};
		    3'b110: // 4 bit zero extend
		        dataExtOut = {{12{1'b0}},dataExtIn[3:0]};
		        
		    default: dataExtOut = 16'd0;
	    endcase
	
	    dataExtOut = dataExtOut<<shiftEn;
	
    end
endmodule
