// synchronous write
// asynchronous read - 2 at a time
module registerFile(
    input clk,
    input [2:0] sourceAddress1,
    input [2:0] sourceAddress2,
    input [2:0] destinationAddress,
    input [15:0] inputData,
    output [15:0] outputData1,
    output [15:0] outputData2);
    
    reg [15:0] register[0:7]; // eight 16-bit register
    
    always @(posedge clk) begin
        register[destinationAddress] <= inputData;
    end
    
    assign outputData1=register[sourceAddress1];
    assign outputData2=register[sourceAddress2];
    
    initial begin
        register[0]=16'd0;
        register[1]=16'd0;
        register[2]=16'd0;
        register[3]=16'd0;
        register[4]=16'd0;
        register[5]=16'd0;
        register[6]=16'd0;
        register[7]=16'd0;
    end
endmodule


