module lc3b(input clk, input clk1);
    wire PCWrite;
    wire [15:0] instruction;
    wire PCSource;
    wire memAddressMuxControl; 
    wire [1:0] memOp;
    wire dataExtInputMux;
    wire [2:0] dataExtControl; 
    wire dataExtOutputMux;
    wire [1:0] ALUIn1MuxControl; 
    wire [2:0] ALUIn2MuxControl;
    wire [2:0] ALUControl;
    wire stripControl;
    wire flagCompEnable;
    wire updateFlags;
    wire [1:0] regDataSelect;
    wire RFSourceAddressMux;
    wire RFWrite;
    wire IRUpdate;
    wire RFDestAddressMux;
    wire ALUOutRegEn;
	
	wire [15:0] PCOut;
	wire [4:0] state;
	
	wire clock_inv;

	wire MDR16En,MDR8En;
	
	assign clock_inv=~clk;
    	
    datapath datapath(clock_inv,PCWrite,PCSource,memAddressMuxControl,memOp,dataExtInputMux,
	    dataExtControl,dataExtOutputMux,ALUIn1MuxControl,ALUIn2MuxControl,ALUControl,
	    stripControl,flagCompEnable,updateFlags,regDataSelect, RFSourceAddressMux,
	    RFWrite,IRUpdate,RFDestAddressMux,ALUOutRegEn,instruction,PCOut,MDR16En,MDR8En);
	
    controller controller(instruction,clock_inv,PCWrite,PCSource,memAddressMuxControl,memOp,dataExtInputMux,
	    dataExtControl,dataExtOutputMux,ALUIn1MuxControl,ALUIn2MuxControl,ALUControl,
	    stripControl,flagCompEnable,updateFlags,regDataSelect, RFSourceAddressMux,
	    RFWrite,IRUpdate,RFDestAddressMux,ALUOutRegEn,state,MDR16En,MDR8En);
	        
endmodule	

