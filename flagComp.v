//Flag comparator

module flagComp (
	input en,
	input[2:0] in_psr,
	input[2:0] in_inst,
	output reg result
	);
	
always@(*) begin

	if(en) begin
		if (in_psr == in_inst) result=1;
		else result=0;
	end
	else result = 1'b0;
	
end
endmodule
