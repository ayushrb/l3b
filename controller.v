module controller(
    input [15:0] instruction,
    input  clock,
    output reg PCWrite,
    output reg PCSource,
    output reg memAddressMuxControl, 
    output reg [1:0] memOp,
    output reg dataExtInputMux,
    output reg [2:0] dataExtControl, 
    output reg dataExtOutputMux,
    output reg [1:0] ALUIn1MuxControl, 
    output reg [2:0] ALUIn2MuxControl,
    output reg [2:0] ALUControl,
    output reg stripControl,
    output reg flagCompEnable,
    output reg updateFlags,
    output reg [1:0] regDataSelect,
    output reg RFSourceAddressMux,
    output reg RFWrite,
    output reg IRUpdate,
    output reg RFDestAddressMux,
    output reg ALUOutRegEn,
	output reg [4:0] state,
	output reg MDR16En,
	output reg MDR8En
    );

    reg [4:0] next_state;
    
    `define no_op_state 5'd31
    `define stop_state 5'd30
    `define fetch_state 5'd0
    `define decode_state 5'd1
    `define add_a_and_b_state 5'd2
    `define write_aluout_to_rf 5'd3
    `define add_a_and_direct_data 5'd4
    `define and_a_and_b_state 5'd5
    `define and_a_and_direct_state 5'd6
    `define xor_a_and_b_state 5'd7
    `define xor_a_and_direct_state 5'd8
    `define branch_state 5'd9
    `define alu_pass1_state 5'd10
    `define write_aluout_to_pc 5'd11
    `define jsr_address_calc_state 5'd12
    // this block assigns the output signals depending upon the current states and input
    always @(*) begin
        if(state==`fetch_state) begin                 // fetch
            PCWrite<=1'b1;
            PCSource<=1'b0;
            memAddressMuxControl<=1'b0;
            memOp<=2'b01;
            dataExtInputMux<=1'b0;
            dataExtControl<=3'b111;
            dataExtOutputMux<=1'b0;
            ALUIn1MuxControl<=2'b00;
            ALUIn2MuxControl<=3'b00;
            ALUControl<=3'b000;
            stripControl<=1'b0;
            flagCompEnable<=1'b0;
            updateFlags<=1'b0;
            regDataSelect<=2'b00;
            RFSourceAddressMux<=1'b0;
            RFWrite<=1'b0;
            IRUpdate<=1'b1;
            RFDestAddressMux<=1'b0;
            ALUOutRegEn<=1'b0;
			MDR16En<=1'b0;
			MDR8En<=1'b0;    
        end      
        else if(state==`decode_state) begin               //decode
            PCWrite<=1'b0;
            PCSource<=1'b0;
            memAddressMuxControl<=1'b0;
            memOp<=2'b10;
            dataExtInputMux<=1'b0;
            dataExtControl<=3'b100;
            dataExtOutputMux<=1'b1;
            ALUIn1MuxControl<=2'b00;
            ALUIn2MuxControl<=3'b001;
            ALUControl<=3'b000;
            stripControl<=1'b0;
            flagCompEnable<=1'b0;
            updateFlags<=1'b0;
            regDataSelect<=2'b00;
            RFSourceAddressMux<=1'b0;
            RFWrite<=1'b0;
            IRUpdate<=1'b0;
            RFDestAddressMux<=1'b0;
            ALUOutRegEn<=1'b1; 
			MDR16En<=1'b0;
			MDR8En<=1'b0; 
        end
        else if(state==`add_a_and_b_state) begin   //ADD A and B
            PCWrite<=1'b0;
            PCSource<=1'b0;
            memAddressMuxControl<=1'b0;
            memOp<=2'b10;
            dataExtInputMux<=1'b0;
            dataExtControl<=3'b111;
            dataExtOutputMux<=1'b0;
            ALUIn1MuxControl<=2'b10;
            ALUIn2MuxControl<=3'b010;
            ALUControl<=3'b000;
            stripControl<=1'b0;
            flagCompEnable<=1'b0;
            updateFlags<=1'b1;
            regDataSelect<=2'b00;
            RFSourceAddressMux<=1'b0;
            RFWrite<=1'b0;
            IRUpdate<=1'b0;
            RFDestAddressMux<=1'b0;
            ALUOutRegEn<=1'b1;
			MDR16En<=1'b0;
			MDR8En<=1'b0;  
        end
    
        else if(state==`write_aluout_to_rf) begin               //RFWrite
            PCWrite<=1'b0;
            PCSource<=1'b0;
            memAddressMuxControl<=1'b0;
            memOp<=2'b10;
            dataExtInputMux<=1'b0;
            dataExtControl<=3'b111;
            dataExtOutputMux<=1'b0;
            ALUIn1MuxControl<=2'b00;
            ALUIn2MuxControl<=3'b000;
            ALUControl<=3'b110;
            stripControl<=1'b0;
            flagCompEnable<=1'b0;
            updateFlags<=1'b0;
            regDataSelect<=2'b10;
            RFSourceAddressMux<=1'b0;
            RFWrite<=1'b1;
            IRUpdate<=1'b0;
            RFDestAddressMux<=1'b0;
            ALUOutRegEn<=1'b0;
			MDR16En<=1'b0;
			MDR8En<=1'b0; 
        end 
        else if(state==`add_a_and_direct_data) begin           // add_immediate 
        
            PCWrite<=1'b0;
            PCSource<=1'b0;
            memAddressMuxControl<=1'b0;
            memOp<=2'b10;
            dataExtInputMux<=1'b0;
            dataExtControl<=3'b001;
            dataExtOutputMux<=1'b0;
            ALUIn1MuxControl<=2'b10;
            ALUIn2MuxControl<=3'b001;
            ALUControl<=3'b000;
            stripControl<=1'b0;
            flagCompEnable<=1'b0;
            updateFlags<=1'b1;
            regDataSelect<=2'b00;
            RFSourceAddressMux<=1'b0;
            RFWrite<=1'b0;
            IRUpdate<=1'b0;
            RFDestAddressMux<=1'b0; 
            ALUOutRegEn<=1'b1;
			MDR16En<=1'b0;
			MDR8En<=1'b0; 
        end
    
        else if(state==`and_a_and_b_state) begin
            PCWrite<=1'b0;
            PCSource<=1'b0;
            memAddressMuxControl<=1'b0;
            memOp<=2'b10;
            dataExtInputMux<=1'b0;
            dataExtControl<=3'b111;
            dataExtOutputMux<=1'b0;
            ALUIn1MuxControl<=2'b10;
            ALUIn2MuxControl<=3'b010;
            ALUControl<=3'b001;
            stripControl<=1'b0;
            flagCompEnable<=1'b0;
            updateFlags<=1'b1;
            regDataSelect<=2'b00;
            RFSourceAddressMux<=1'b0;
            RFWrite<=1'b0;
            IRUpdate<=1'b0;
            RFDestAddressMux<=1'b0;
            ALUOutRegEn<=1'b1; 
            MDR16En<=1'b0;
			MDR8En<=1'b0; 
        end
    
    
        else if(state==`and_a_and_direct_state) begin //AND Imm.
            PCWrite<=1'b0;
            PCSource<=1'b0;
            memAddressMuxControl<=1'b0;
            memOp<=2'b10;
            dataExtInputMux<=1'b0;
            dataExtControl<=3'b001;
            dataExtOutputMux<=1'b0;
            ALUIn1MuxControl<=2'b10;
            ALUIn2MuxControl<=3'b001;
            ALUControl<=3'b001;
            stripControl<=1'b0;
            flagCompEnable<=1'b0;
            updateFlags<=1'b1;
            regDataSelect<=2'b00;
            RFSourceAddressMux<=1'b0;
            RFWrite<=1'b0;
            IRUpdate<=1'b0;
            RFDestAddressMux<=1'b0; 
            ALUOutRegEn<=1'b1;
            MDR16En<=1'b0;
			MDR8En<=1'b0; 
        end
    
        else if(state==`xor_a_and_b_state) begin       //XOR Direct
            PCWrite<=1'b0;
            PCSource<=1'b0;
            memAddressMuxControl<=1'b0;
            memOp<=2'b10;
            dataExtInputMux<=1'b0;
            dataExtControl<=3'b111;
            dataExtOutputMux<=1'b0;
            ALUIn1MuxControl<=2'b10;
            ALUIn2MuxControl<=3'b010;
            ALUControl<=3'b010;
            stripControl<=1'b0;
            flagCompEnable<=1'b0;
            updateFlags<=1'b1;
            regDataSelect<=2'b00;
            RFSourceAddressMux<=1'b0;
            RFWrite<=1'b0;
            IRUpdate<=1'b0;
            RFDestAddressMux<=1'b0;
            ALUOutRegEn<=1'b1;
            MDR16En<=1'b0;
			MDR8En<=1'b0;  
        end
    
    
        else if(state==`xor_a_and_direct_state) begin            //XOR Immediate
            PCWrite<=1'b0;
            PCSource<=1'b0;
            memAddressMuxControl<=1'b0;
            memOp<=2'b10;
            dataExtInputMux<=1'b0;
            dataExtControl<=3'b001;
            dataExtOutputMux<=1'b0;
            ALUIn1MuxControl<=2'b10;
            ALUIn2MuxControl<=3'b001;
            ALUControl<=3'b010;
            stripControl<=1'b0;
            flagCompEnable<=1'b0;
            updateFlags<=1'b1;
            regDataSelect<=2'b00;
            RFSourceAddressMux<=1'b0;
            RFWrite<=1'b0;
            IRUpdate<=1'b0; 
            RFDestAddressMux<=1'b0;
            ALUOutRegEn<=1'b1;
            MDR16En<=1'b0;
			MDR8En<=1'b0; 
        end
    
    
        else if(state==`branch_state) begin //Branch     ... Maybe some problem.. we need to OR PC Write & CC
            PCWrite<=1'b0;
            PCSource<=1'b1;
            memAddressMuxControl<=1'b0;
            memOp<=2'b10;
            dataExtInputMux<=1'b0;
            dataExtControl<=3'b111;
            dataExtOutputMux<=1'b0;
            ALUIn1MuxControl<=2'b00;
            ALUIn2MuxControl<=3'b000;
            ALUControl<=3'b110;
            stripControl<=1'b0;
            flagCompEnable<=1'b1;
            updateFlags<=1'b0;
            regDataSelect<=2'b00;
            RFSourceAddressMux<=1'b0;
            RFWrite<=1'b0;
            IRUpdate<=1'b0;
            RFDestAddressMux<=1'b0;
            ALUOutRegEn<=1'b0;
            MDR16En<=1'b0;
			MDR8En<=1'b0; 
        end
    
        else if(state==`alu_pass1_state) begin //Jump (ALU_0ut <- ALU Pass1)

            PCWrite<=1'b0;
            PCSource<=1'b0;
            memAddressMuxControl<=1'b0;
            memOp<=2'b10;
            dataExtInputMux<=1'b0;
            dataExtControl<=3'b111;
            dataExtOutputMux<=1'b0;
            ALUIn1MuxControl<=2'b10;
            ALUIn2MuxControl<=3'b000;
            ALUControl<=3'b011;
            stripControl<=1'b0;
            flagCompEnable<=1'b0;
            updateFlags<=1'b0;
            regDataSelect<=2'b00;
            RFSourceAddressMux<=1'b0;
            RFWrite<=1'b0;
            IRUpdate<=1'b0;
            RFDestAddressMux<=1'b0;
            ALUOutRegEn<=1'b1;
            MDR16En<=1'b0;
			MDR8En<=1'b0; 
        end
    
        else if(state==`write_aluout_to_pc) begin //PC <- ALU_out

            PCWrite<=1'b1;
            PCSource<=1'b1;
            memAddressMuxControl<=1'b0;
            memOp<=2'b10;
            dataExtInputMux<=1'b0;
            dataExtControl<=3'b111;
            dataExtOutputMux<=1'b0;
            ALUIn1MuxControl<=2'b00;
            ALUIn2MuxControl<=3'b000;
            ALUControl<=3'b110;
            stripControl<=1'b0;
            flagCompEnable<=1'b0;
            updateFlags<=1'b0;
            regDataSelect<=2'b00;
            RFSourceAddressMux<=1'b0;
            RFWrite<=1'b0;
            IRUpdate<=1'b0;
            RFDestAddressMux<=1'b0;
            ALUOutRegEn<=1'b0;
            MDR16En<=1'b0;
			MDR8En<=1'b0; 
        end
    
        else if(state==`jsr_address_calc_state) begin   //JSR
            PCWrite<=1'b0;
            PCSource<=1'b0;
            memAddressMuxControl<=1'b0;
            memOp<=2'b10;
            dataExtInputMux<=1'b0;
            dataExtControl<=3'b101;
            dataExtOutputMux<=1'b1;
            ALUIn1MuxControl<=2'b00;
            ALUIn2MuxControl<=3'b001;
            ALUControl<=3'b000;
            stripControl<=1'b0;
            flagCompEnable<=1'b0;
            updateFlags<=1'b0;
            regDataSelect<=2'b00;
            RFSourceAddressMux<=1'b0;
            RFWrite<=1'b1;
            IRUpdate<=1'b0;
            RFDestAddressMux<=1'b1;
            ALUOutRegEn<=1'b1;
            MDR16En<=1'b0;
			MDR8En<=1'b0; 
        end 
        
        else if(state==13) begin    //LDB -- ALU_OUT <- A + Signextend(IR[5:0])
            PCWrite<=1'b0;
            PCSource<=1'b0;
            memAddressMuxControl<=1'b0;
            memOp<=2'b10;
            dataExtInputMux<=1'b0;
            dataExtControl<=3'b010;
            dataExtOutputMux<=1'b0;
            ALUIn1MuxControl<=2'b10;
            ALUIn2MuxControl<=3'b001;
            ALUControl<=3'b000;
            stripControl<=1'b0;
            flagCompEnable<=1'b0;
            updateFlags<=1'b0;
            regDataSelect<=2'b00;
            RFSourceAddressMux<=1'b0;
            RFWrite<=1'b0;
            IRUpdate<=1'b0;
            RFDestAddressMux<=1'b0;
            ALUOutRegEn<=1'b1;
            MDR16En<=1'b0;
			MDR8En<=1'b0; 
        end
    
        else if(state==14) begin //ALU_OUT,Reg(IR[11:9] <- signextend(MDR) 
    
            PCWrite<=1'b0;
            PCSource<=1'b0;
            memAddressMuxControl<=1'b0;
            memOp<=2'b10;
            dataExtInputMux<=1'b1;
            dataExtControl<=3'b011;
            dataExtOutputMux<=1'b0;
            ALUIn1MuxControl<=2'b00;
            ALUIn2MuxControl<=3'b001;
            ALUControl<=3'b110;
            stripControl<=1'b0;
            flagCompEnable<=1'b0;
            updateFlags<=1'b1;
            regDataSelect<=2'b11;
            RFSourceAddressMux<=1'b0;
            RFWrite<=1'b1;
            IRUpdate<=1'b0;
            RFDestAddressMux<=1'b0;
            ALUOutRegEn<=1'b0;
            MDR16En<=1'b0;
			MDR8En<=1'b0; 
        end
    
        else if(state==15) begin   //MDR <- mem[ALU_OUT] //***CHANGE*** Byte read
    
            PCWrite<=1'b0;
            PCSource<=1'b0;
            memAddressMuxControl<=1'b1;
            memOp<=2'b00;
            dataExtInputMux<=1'b0;
            dataExtControl<=3'b111;
            dataExtOutputMux<=1'b0;
            ALUIn1MuxControl<=2'b00;
            ALUIn2MuxControl<=3'b000;
            ALUControl<=3'b110;
            stripControl<=1'b0;
            flagCompEnable<=1'b0;
            updateFlags<=1'b0;
            regDataSelect<=2'b00;
            RFSourceAddressMux<=1'b0;
            RFWrite<=1'b0;
            IRUpdate<=1'b0;
            RFDestAddressMux<=1'b0;
            ALUOutRegEn<=1'b0;
            MDR16En<=1'b0;
			MDR8En<=1'b1; 
        end
    
    
        else if(state==16) begin //ALUOut ← A +LSHF(SignExtend(IR[5:0]))
            PCWrite<=1'b0;
            PCSource<=1'b0;
            memAddressMuxControl<=1'b0;
            memOp<=2'b10;
            dataExtInputMux<=1'b0;
            dataExtControl<=3'b010;
            dataExtOutputMux<=1'b1;
            ALUIn1MuxControl<=2'b10;
            ALUIn2MuxControl<=3'b001;
            ALUControl<=3'b000;
            stripControl<=1'b0;
            flagCompEnable<=1'b0;
            updateFlags<=1'b0;
            regDataSelect<=2'b00;
            RFSourceAddressMux<=1'b0;
            RFWrite<=1'b0;
            IRUpdate<=1'b0;
            RFDestAddressMux<=1'b0;
            ALUOutRegEn<=1'b1;
            MDR16En<=1'b0;
			MDR8En<=1'b0; 
        end
        else if(state==17) begin //word read
            PCWrite<=1'b0;
            PCSource<=1'b0;
            memAddressMuxControl<=1'b1;
            memOp<=2'b01;
            dataExtInputMux<=1'b0;
            dataExtControl<=3'b111;
            dataExtOutputMux<=1'b0;
            ALUIn1MuxControl<=2'b00;
            ALUIn2MuxControl<=3'b000;
            ALUControl<=3'b110;
            stripControl<=1'b0;
            flagCompEnable<=1'b0;
            updateFlags<=1'b0;
            regDataSelect<=2'b00;
            RFSourceAddressMux<=1'b0;
            RFWrite<=1'b0;
            IRUpdate<=1'b0;
            RFDestAddressMux<=1'b0;
            ALUOutRegEn<=1'b0;
            MDR16En<=1'b1;
			MDR8En<=1'b0; 
        end
        
        else if(state==18) begin
            PCWrite<=1'b0;
            PCSource<=1'b0;
            memAddressMuxControl<=1'b0;
            memOp<=2'b10;
            dataExtInputMux<=1'b1;
            dataExtControl<=3'b011;
            dataExtOutputMux<=1'b0;
            ALUIn1MuxControl<=2'b00;
            ALUIn2MuxControl<=3'b100;
            ALUControl<=3'b110;
            stripControl<=1'b0;
            flagCompEnable<=1'b0;
            updateFlags<=1'b1;
            regDataSelect<=2'b01;
            RFSourceAddressMux<=1'b0;
            RFWrite<=1'b1;
            IRUpdate<=1'b0;
            RFDestAddressMux<=1'b0;
            ALUOutRegEn<=1'b0;
            MDR16En<=1'b0;
			MDR8En<=1'b0; 
        end
        
        else if(state==19) begin            //M[ALUout] ← Strip(Reg(IR[11:9]),LOW)
            PCWrite<=1'b0;
            PCSource<=1'b0;
            memAddressMuxControl<=1'b1;
            memOp<=2'b11;
            dataExtInputMux<=1'b0;
            dataExtControl<=3'b111;
            dataExtOutputMux<=1'b0;
            ALUIn1MuxControl<=2'b00;
            ALUIn2MuxControl<=3'b000;
            ALUControl<=3'b110;
            stripControl<=1'b0;
            flagCompEnable<=1'b0;
            updateFlags<=1'b0;
            regDataSelect<=2'b00;
            RFSourceAddressMux<=1'b1;
            RFWrite<=1'b0;
            IRUpdate<=1'b0;
            RFDestAddressMux<=1'b0;
            ALUOutRegEn<=1'b0;
            MDR16En<=1'b0;
			MDR8En<=1'b0; 
        end
    
        else if(state==20) begin              //M[ALUout] ← Strip(Reg(IR[11:9]),HIGH)
            PCWrite<=1'b0;
            PCSource<=1'b0;
            memAddressMuxControl<=1'b1;
            memOp<=2'b11;
            dataExtInputMux<=1'b0;
            dataExtControl<=3'b111;
            dataExtOutputMux<=1'b0;
            ALUIn1MuxControl<=2'b00;
            ALUIn2MuxControl<=3'b000;
            ALUControl<=3'b110;
            stripControl<=1'b1;
            flagCompEnable<=1'b0;
            updateFlags<=1'b0;
            regDataSelect<=2'b00;
            RFSourceAddressMux<=1'b1;
            RFWrite<=1'b0;
            IRUpdate<=1'b0;
            RFDestAddressMux<=1'b0;
            ALUOutRegEn<=1'b0;
            MDR16En<=1'b0;
			MDR8En<=1'b0; 
        end

        else if(state==21) begin //ALUOUT = ALUOUT + 1
            PCWrite<=1'b0;
            PCSource<=1'b0;
            memAddressMuxControl<=1'b0;
            memOp<=2'b10;
            dataExtInputMux<=1'b0;
            dataExtControl<=3'b111;
            dataExtOutputMux<=1'b0;
            ALUIn1MuxControl<=2'b01;
            ALUIn2MuxControl<=3'b011;
            ALUControl<=3'b000;
            stripControl<=1'b0;
            flagCompEnable<=1'b0;
            updateFlags<=1'b1;
            regDataSelect<=2'b00;
            RFSourceAddressMux<=1'b0;
            RFWrite<=1'b0;
            IRUpdate<=1'b0;
            RFDestAddressMux<=1'b0;
            ALUOutRegEn<=1'b1;
            MDR16En<=1'b0;
			MDR8En<=1'b0; 
        end
    
        else if(state==22) begin    //ALUOut <-- ALUPass1 (setCC)
            PCWrite<=1'b0;
            PCSource<=1'b0;
            memAddressMuxControl<=1'b0;
            memOp<=2'b10;
            dataExtInputMux<=1'b0;
            dataExtControl<=3'b111;
            dataExtOutputMux<=1'b0;
            ALUIn1MuxControl<=2'b01;
            ALUIn2MuxControl<=3'b000;
            ALUControl<=3'b011;
            stripControl<=1'b0;
            flagCompEnable<=1'b0;
            updateFlags<=1'b1;
            regDataSelect<=2'b00;
            RFSourceAddressMux<=1'b0;
            RFWrite<=1'b0;
            IRUpdate<=1'b0;
            RFDestAddressMux<=1'b0;
            ALUOutRegEn<=1'b0;
            MDR16En<=1'b0;
			MDR8En<=1'b0; 
        end
    
        else if(state==23) begin           //A LSHF IR[3:0] 
            PCWrite<=1'b0;
            PCSource<=1'b0;
            memAddressMuxControl<=1'b0;
            memOp<=2'b10;
            dataExtInputMux<=1'b0;
            dataExtControl<=3'b110;
            dataExtOutputMux<=1'b0;
            ALUIn1MuxControl<=2'b10;
            ALUIn2MuxControl<=3'b001;
            ALUControl<=3'b100;
            stripControl<=1'b0;
            flagCompEnable<=1'b0;
            updateFlags<=1'b1;
            regDataSelect<=2'b00;
            RFSourceAddressMux<=1'b0;
            RFWrite<=1'b0;
            IRUpdate<=1'b0;
            RFDestAddressMux<=1'b0;
            ALUOutRegEn<=1'b1;
            MDR16En<=1'b0;
			MDR8En<=1'b0; 
        end
    
        else if(state==24) begin           //A RSHFL IR[3:0]
            PCWrite<=1'b0;
            PCSource<=1'b0;
            memAddressMuxControl<=1'b0;
            memOp<=2'b10;
            dataExtInputMux<=1'b0;
            dataExtControl<=3'b110;
            dataExtOutputMux<=1'b0;
            ALUIn1MuxControl<=2'b10;
            ALUIn2MuxControl<=3'b001;
            ALUControl<=3'b101;
            stripControl<=1'b0;
            flagCompEnable<=1'b0;
            updateFlags<=1'b1;
            regDataSelect<=2'b00;
            RFSourceAddressMux<=1'b0;
            RFWrite<=1'b0;
            IRUpdate<=1'b0;
            RFDestAddressMux<=1'b0;
            ALUOutRegEn<=1'b1;
            MDR16En<=1'b0;
			MDR8En<=1'b0; 
        end
    
        else if(state==25) begin           //A RSHFA B 
            PCWrite<=1'b0;
            PCSource<=1'b0;
            memAddressMuxControl<=1'b0;
            memOp<=2'b10;
            dataExtInputMux<=1'b0;
            dataExtControl<=3'b110;
            dataExtOutputMux<=1'b0;
            ALUIn1MuxControl<=2'b10;
            ALUIn2MuxControl<=3'b001;
            ALUControl<=3'b111;
            stripControl<=1'b0;
            flagCompEnable<=1'b0;
            updateFlags<=1'b1;
            regDataSelect<=2'b00;
            RFSourceAddressMux<=1'b0;
            RFWrite<=1'b0;
            IRUpdate<=1'b0;
            RFDestAddressMux<=1'b0;
            ALUOutRegEn<=1'b1;
            MDR16En<=1'b0;
			MDR8En<=1'b0; 
        end
        else if(state==26) begin
            PCWrite<=1'b0;
            PCSource<=1'b0;
            memAddressMuxControl<=1'b0;
            memOp<=2'b10;
            dataExtInputMux<=1'b0;
            dataExtControl<=3'b111;
            dataExtOutputMux<=1'b0;
            ALUIn1MuxControl<=2'b10;
            ALUIn2MuxControl<=3'b000;
            ALUControl<=3'b011;
            stripControl<=1'b0;
            flagCompEnable<=1'b0;
            updateFlags<=1'b0;
            regDataSelect<=2'b00;
            RFSourceAddressMux<=1'b0;
            RFWrite<=1'b1;
            IRUpdate<=1'b0;
            RFDestAddressMux<=1'b1;
            ALUOutRegEn<=1'b1;
            MDR16En<=1'b0;
			MDR8En<=1'b0; 
        end
            
        else begin      //no_op_state and stop_state
            PCWrite<=1'b0;
            PCSource<=1'b0;
            memAddressMuxControl<=1'b0;
            memOp<=2'b10;
            dataExtInputMux<=1'b0;
            dataExtControl<=3'b111;
            dataExtOutputMux<=1'b0;
            ALUIn1MuxControl<=2'b00;
            ALUIn2MuxControl<=3'b000;
            ALUControl<=3'b110;
            stripControl<=1'b0;
            flagCompEnable<=1'b0;
            updateFlags<=1'b0;
            regDataSelect<=2'b00;
            RFSourceAddressMux<=1'b0;
            RFWrite<=1'b0;
            IRUpdate<=1'b0;
            RFDestAddressMux<=1'b0;
            ALUOutRegEn<=1'b0;
            MDR16En<=1'b0;
			MDR8En<=1'b0; 
        end
        
        
        //deciding the next state
        //instruction independented states)
        if(state==`no_op_state) begin
            //fetch the new instruction
            next_state<=0;                
        end
        else if(state==0) begin
            next_state<=1;
        end 
        else if(state==`stop_state) begin
            // do nothing
            next_state<=`stop_state;
        end
        else begin  //instruction dependent instructions
            if(instruction[15:12]==4'b0001 && instruction[5]==0) begin //ADD reg
                case(state)
                    1:              next_state<=2;
                    2:              next_state<=3;
                    3:              next_state<=0;
					default:		next_state<=0;
                endcase
            end
            else if(instruction[15:12]==4'b0001 && instruction[5]==1) begin //ADD immediate
                case(state)            
                    1:              next_state<=4;
                    4:              next_state<=3;
                    3:              next_state<=0;
					default:		next_state<=0;
                endcase
            end
            else if(instruction[15:12]==4'b0101 && instruction[5]==0) begin // AND reg
                case(state)            
                    1:              next_state<=5;
                    5:              next_state<=3;
                    3:              next_state<=0;
					default:		next_state<=0;
                endcase
            end
            else if(instruction[15:12]==4'b0101 && instruction[5]==1) begin // AND immediate
                case(state)            
                    1:              next_state<=6;
                    6:              next_state<=3;
                    3:              next_state<=0;
					default:		next_state<=0;
                endcase
            end
            else if(instruction[15:12]==4'b1001 && instruction[5]==0) begin // XOR reg
                case(state)            
                    1:              next_state<=7;
                    7:              next_state<=3;
                    3:              next_state<=0;
					default:		next_state<=0;
                endcase
            end
            else if(instruction[15:12]==4'b1001 && instruction[5]==1) begin // XOR immediate
				case(state)            
                    1:              next_state<=8;
                    8:              next_state<=3;
                    3:              next_state<=0;
					default:		next_state<=0;
                endcase
            end
            else if(instruction[15:12]==4'b1100) begin  //JMP and RET
                case(state)            
                    1:              next_state<=10;
                    10:             next_state<=11;
                    11:             next_state<=0;
					default:		next_state<=0;
                endcase
            end
            else if(instruction[15:12]==4'b0100 && instruction[11]==1'b1) begin //JSR
                case(state)            
                    1:              next_state<=12;
                    12:             next_state<=11;
                    11:             next_state<=0;
					default:		next_state<=0;
                endcase
            end
            else if(instruction[15:12]==4'b0100 && instruction[11]==1'b0) begin    //JSRR
                case(state)            
                    1:              next_state<=26;
                    26:             next_state<=11;
                    11:             next_state<=0;
					default:		next_state<=0;
                endcase
            end
            else if(instruction[15:12]==4'b0010) begin// LDB
                case(state) 
                    1:              next_state<=13;
                    13:             next_state<=15;
                    15:             next_state<=14;
                    14:             next_state<=0;
					default:		next_state<=0;
                endcase
            end
            else if(instruction[15:12]==4'b0110) begin //LDW
                case(state) 
                    1:              next_state<=16;
                    16:             next_state<=17;
                    17:             next_state<=18;
                    18:             next_state<=0;
					default:		next_state<=0;
                endcase
            end
            else if(instruction[15:12]==4'b0011) begin //STB
                case(state)            
                    1:              next_state<=13;
                    13:             next_state<=19;
                    19:             next_state<=0;
					default:		next_state<=0;
                endcase
            end
            else if(instruction[15:12]==4'b0111) begin //STW
                case(state)            
                    1:              next_state<=16;
                    16:             next_state<=19;
                    19:             next_state<=21;
                    21:             next_state<=20;
                    20:             next_state<=0;
					default:		next_state<=0;
                endcase
            end
            else if(instruction[15:12]==4'b0000) begin //Branch
                case(state)            
                    1:              next_state<=9;
                    9:              next_state<=0;  
					default:		next_state<=0;
                endcase
            end
            else if(instruction[15:12]==4'b1110) begin //LEA
                case(state)            
                    1:              next_state<=3;
                    3:              next_state<=22;
                    22:             next_state<=0;
					default:		next_state<=0;
                endcase
            end
            else if(instruction[15:12]==4'b1101 && instruction[5:4]==2'b00) begin // LSHF  
                case(state)            
                    1:              next_state<=23;
                    23:             next_state<=3;
                    3:              next_state<=0;
					default:		next_state<=0;
                endcase
            end
            else if(instruction[15:12]==4'b1101 && instruction[5:4]==2'b01) begin // RSHFL  
                case(state)            
                    1:              next_state<=24;
                    24:             next_state<=3;
                    3:              next_state<=0;
					default:		next_state<=0;
                endcase
            end
            else if(instruction[15:12]==4'b1101 && instruction[5:4]==2'b11) begin // RSHFA  
                case(state)            
                    1:              next_state<=25;
                    25:             next_state<=3;
                    3:              next_state<=0;
					default:		next_state<=0;
                endcase
            end
            else begin
                next_state<=`stop_state; //stop the execution
            end
        end         
    end     


    always@(negedge clock) begin
        state<=next_state;
    end
    
    initial begin
        state<=`no_op_state;
    end
    
endmodule

/*module controller_test;
    reg [15:0] instruction;
    reg clock;
    wire PCWrite;
    wire PCSource;
    wire memAddressMuxControl; 
    wire [1:0] memOp;
    wire dataExtInputMux;
    wire [2:0] dataExtControl; 
    wire dataExtOutputMux;
    wire [1:0] ALUIn1MuxControl; 
    wire [2:0] ALUIn2MuxControl;
    wire [2:0] ALUControl;
    wire stripControl;
    wire flagCompEnable;
    wire updateFlags;
    wire [1:0] regDataSelect;
    wire RFSourceAddressMux;
    wire RFWrite;
    wire IRUpdate;
    wire RFDestAddressMux;
    
    controller dut (instruction,clock,PCWrite, PCSource, memAddressMuxControl,memOp,dataExtInputMux,
        dataExtControl,dataExtOutputMux,ALUIn1MuxControl,ALUIn2MuxControl,ALUControl,
        stripControl,flagCompEnable,updateFlags,regDataSelect,RFSourceAddressMux,RFWrite,IRUpdate,RFDestAddressMux);

    initial begin
        $dumpfile("controller_test.vcd");
        $dumpvars();
        
        instruction = 16'b0000_001_000001100;
        clock=1;
        
        #90 $finish;
    end
    
    always #5 clock=!clock;
    
endmodule*/
