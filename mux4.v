//4 input MUX

module mux4 (
	input [1:0] sel,
	input [15:0] in1,
	input [15:0] in2,
	input [15:0] in3,
	input [15:0] in4,
	output reg [15:0] out
	);
	
    always@(*) begin

	    case (sel)
		    2'b00: out=in1;
		    2'b01: out=in2;
		    2'b10: out=in3;
		    2'b11: out=in4;
	    endcase
	
    end
endmodule
