//PSR
// flagReg={n,z,p}

module flagReg (
	input clk,
	input [2:0] in,
	output [2:0] out
	);
	
	reg [2:0] flagReg;
	
	assign out=flagReg;
    always@(posedge clk) begin
        flagReg=in;
    end

endmodule
