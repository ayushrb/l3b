//8 bit register

module reg8Bit (
	input [7:0] in,
	input clk,
	output reg [7:0] out
	);
	
    always @(posedge clk) begin
	    out = in;
    end

endmodule
