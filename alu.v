// Ayush Baid
// 12D1000002

/*
* performs the calculation on the positive edge of the clock
* two 16-bit inputs, one 16-bit output
* conditional codes (n/z/p are set depending upon the result
*/
module alu(
    input [15:0] alu_in_1,
    input [15:0] alu_in_2,
    input [2:0] alu_control,
    output reg [15:0] alu_out,
    output reg [2:0] cc);
    
    `define addControl 3'b000
    `define andControl 3'b001
    `define xorControl 3'b010
    `define pass1Control 3'b011
    `define pass2Control 3'b110
    `define shiftLeftLogicalControl 3'b100
    `define shiftRightLogicalControl 3'b101
    `define shiftRightArithmeticControl 3'b111
    
    //use 110 for no op
    
    
    
    always @(*) begin
        case(alu_control)
            `addControl:
                alu_out<=alu_in_1+alu_in_2;
            `andControl:
                alu_out<=alu_in_1 & alu_in_2;
            `xorControl:
                alu_out<=alu_in_1 ^ alu_in_2;
            `shiftLeftLogicalControl:
                alu_out<=alu_in_1 << alu_in_2;
            `shiftRightLogicalControl:
                alu_out<=alu_in_1 >> alu_in_2;
            `shiftRightArithmeticControl: begin
				alu_out<=$signed(alu_in_1) >>> alu_in_2;
			end
            `pass1Control:
                alu_out<=alu_in_1;
            `pass2Control:
                alu_out<=alu_in_2;
            default:begin end
        endcase
        
        if(alu_out<0)
            cc<=3'b100;
        else if(alu_out==0)
            cc<=3'b010;
        else
            cc<=3'b001;
		
    end
endmodule
