//Datapath
// Author - Ayush

module datapath(clock,PCWrite,PCSource,memAddressMuxControl,memOp,dataExtInputMux,
	dataExtControl,dataExtOutputMux,ALUIn1MuxControl,ALUIn2MuxControl,ALUControl,
	stripControl,flagCompEnable,updateFlags,regDataSelect, RFSourceAddressMux,
	RFWrite,IRUpdate,RFDestAddressMux,ALUOutRegEn,IROut,PCOut,MDR16En,MDR8En);       //added IRout
    
    input clock;
    input PCWrite;         			// to update PC
    input PCSource;         		// to select the source for PC update
    input memAddressMuxControl;
    input [1:0] memOp;            	// memory operation R/Wb
    
    // control signals for signextension
    input dataExtInputMux;
    input [2:0] dataExtControl;
    input dataExtOutputMux;
	
    input [1:0] ALUIn1MuxControl;
    input [2:0] ALUIn2MuxControl;
    input [2:0] ALUControl;
    
    input stripControl;    // 0-Lower byte ; 1-upper
    input flagCompEnable;
    input updateFlags;
    
    input [1:0] regDataSelect;
    input RFSourceAddressMux;
    input RFWrite;
	input IRUpdate;
	input RFDestAddressMux;
	input ALUOutRegEn;
	output [15:0] IROut;            //added IROut here 
    
	output [15:0] PCOut;
	input MDR16En,MDR8En;
	
    wire flagCompResult;  

    wire [2:0] const_7;
    wire [15:0] const_2;
    wire [15:0] const_1;
    // wires for ALU
    wire [2:0] CCWire;
    
    // wires for PC
    wire [15:0]PCInput;
    wire PCClock,PSRClock;
    wire [15:0] memAddress,memWordOut,MDRWordOut,RFDataOut1,RFDataOut2, //removed IROut from here 
		RFDataIn,RegAOut,RegBOut,ALUIn1,ALUIn2,ALUOut,ALUOutRegOut;
    wire [7:0] memData,memByteOut,MDRByteOut;
    wire IRClock,RFWriteClock,ALUOutRegClock;
    wire [2:0] RFSourceAddress2,RFDestAddress;
    
    //Wires for Data Extend
	wire [15:0] dataExtIn;
    wire [15:0] dataExtOut;
    
    //Wires for PSR
	wire [2:0] PSROut;
    
    //wires for MUX
    
    
    // PC
    mux2 PCSourceMux(ALUOut,ALUOutRegOut,PCSource,PCInput);
    assign PCClock = clock & (PCWrite | flagCompResult);   // to control by branch
    reg16Bit PCRegister(PCInput, PCClock, PCOut);
    
    // selects the address for memory block - 2 bit control
    mux2  memAddressMux(PCOut,ALUOutRegOut,memAddressMuxControl,memAddress);
    
    /*
    * memory block
    * 512 bytes of 8bit memory
    * async read (both byte and word_ and sync write (depending on control signal - memOp (2 bits))
    */
    memory  memoryObject(memAddress,memData,clock,memOp,memByteOut,memWordOut);
        
    // MDR for both byte and word
    wire mem16clock = clock & MDR16En;
    wire mem8clock = clock & MDR8En;
    reg8Bit byteMDR(memByteOut,mem8clock,MDRByteOut);
    reg16Bit wordMDR(memWordOut,mem16clock,MDRWordOut);
    
    // Instruction Register; gets updated only when IR signal is high
    assign IRClock=clock&IRUpdate;
    reg16Bit IR(memWordOut,IRClock,IROut);
    
    //register files
    assign RFWriteClock=clock & RFWrite;
	mux2_3bit RFSourceAdrr2Mux(IROut[2:0],IROut[11:9], RFSourceAddressMux,RFSourceAddress2);
	mux2_3bit RFDestAdrrMux(IROut[11:9],const_7,RFDestAddressMux,RFDestAddress);
	mux4 RFDataInputMux(regDataSelect,PCOut,MDRWordOut,ALUOutRegOut,dataExtOut,RFDataIn);
    registerFile RF(RFWriteClock,IROut[8:6],RFSourceAddress2,RFDestAddress,RFDataIn,RFDataOut1,RFDataOut2);
    
    // A and B registers
    reg16Bit RegA(RFDataOut1,clock,RegAOut);
    reg16Bit RegB(RFDataOut2,clock,RegBOut);
    
    // ALU
    mux4 ALUIn1Mux(ALUIn1MuxControl,PCOut,ALUOutRegOut,RegAOut,const_1,ALUIn1);
    mux_5In ALUIn2Mux(const_2,dataExtOut,RegBOut,const_1,MDRWordOut,ALUIn2MuxControl,ALUIn2);
    
    alu aluObject(ALUIn1,ALUIn2,ALUControl,ALUOut,CCWire);
    
    //writing to ALUOut
    assign ALUOutRegClock=clock & ALUOutRegEn;
    reg16Bit ALUOutReg(ALUOut,ALUOutRegClock,ALUOutRegOut);
    
    //data Extension along with input and output muxes
    mux2 DataExtMux(IROut[10:0], MDRByteOut, dataExtInputMux, dataExtIn);
    data_ext DataExt (dataExtControl, dataExtIn, dataExtOutputMux, dataExtOut);
    
    // to store the flag
    // update performed only when updateFlags=1
    assign PSRClock=clock & updateFlags;
    flagReg PSR(PSRClock, CCWire, PSROut);
	
	// for generating 8 bits from 16 bits
	strip strip(RegBOut,stripControl,memData);
    
    flagComp flag_comp (flagCompEnable, PSROut, IROut[11:9], flagCompResult);
    
    
    
    assign const_7=3'b111;     // to be used as address to R7
	assign const_2=16'b0000_0000_0000_0010;
	assign const_1=16'b0000_0000_0000_0001;
	
endmodule



