//16 bit register

module reg16Bit (
	input [15:0] in,
	input clk,
	output reg [15:0] out
	);
	
    always @(posedge clk) begin
	    out = in;
    end
    
    initial begin
        out=16'd0;
    end

endmodule
