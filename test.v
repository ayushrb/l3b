module test;
    reg clk;
	reg clk1;
    
    lc3b dut(clk,clk1);
    
    initial begin
        $dumpfile("test.vcd");
        $dumpvars();
        clk=1;
        
        #600 $finish;
    end
    
    always #5 clk=~clk;
endmodule
        
