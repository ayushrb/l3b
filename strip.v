//Strip

module strip (
	input [15:0] strip_in,
	input strip_control,
	output reg [7:0] strip_out
	);

always@(*) begin
	if (strip_control) strip_out = strip_in [15:8];

	else strip_out = strip_in [7:0];

end
endmodule
