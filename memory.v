//Memory
// async word(01) and byte(00) read
// sync byte write (1x)

module memory (
	input [15:0] addIn,
	input [7:0] dataIn,
	input clk,
	input [1:0]memOp,
	output reg [7:0] byteOut,
	output reg [15:0] wordOut
	);
	
    reg [7:0] register[0:64]; // 64 8-bit registers
    
    always @(posedge clk) begin
	    if(memOp==2'b11) begin
		    register[addIn[5:0]]<= dataIn;
	    end
	    else begin
	    end
    end
    always @(*) begin
       if(memOp==2'b00) begin
          byteOut<=register[addIn[5:0]];
          wordOut<=16'h0000;
       end
       else if(memOp==2'b01) begin
            byteOut<=8'h00;
            if(addIn[0]==0) begin
        		wordOut[7:0]<=register[addIn[5:0]];
        		wordOut[15:8]<=register[addIn[5:0]+1];
        	end
        	else begin
        	    wordOut<=16'h0000;
        	end
	    end
	    else begin
		    byteOut<=8'h00;
		    wordOut<=16'h0000;
		end
    end
    
    initial begin
        
		//DATA:
		{register[41],register[40]}<=16'd38;
		{register[43],register[42]}<=16'd1000;
		register[16]<=8'd52;
		register[17]<=8'd12;
		
		
		//CODE:
        {register[1],register[0]}<=16'b0110_000_011_010100;      // Load the MEM[41,40] into RF[0]
        {register[3],register[2]}<=16'b0110_001_011_010101;      // Load the MEM[43,42] into RF[1]
        {register[5],register[4]}<=16'b0010_010_011_010000;      // Load the MEM[16] into RF[2]
        {register[7],register[6]}<=16'b0001_000_000_0_00_001;    // RF[0]=RF[0]+RF[1]
        {register[9],register[8]}<=16'b0001_001_000_1_01111;     // RF[1]=RF[0]+#15
        {register[11],register[10]}<=16'b0000_001_000001001;     // Branch to #30 if positive
        {register[31],register[30]}<=16'b0101_010_010_1_01100;   // RF[2]=RF[2]&01100
        {register[33],register[32]}<=16'b0010_100_011_010001;    // RF[4]=mem[17]
        {register[35],register[34]}<=16'b1100_000_100_000000;    // JMP to RF[4]
        {register[13],register[12]}<=16'b0100_1_00000000110;     // JSR to #26
        {register[27],register[26]}<=16'b1101_000_000_0_0_0010;  // RF[0]=RF[0]<<2
        {register[29],register[28]}<=16'b1100_000_111_000000;    // RET
        
        

		// no op
		{register[15],register[14]}<=16'b1010_000_000_000000;
	end
        
endmodule
