//2 input mux
//16 bit data

module mux2 (
	input [15:0] mux2In1,
	input [15:0] mux2In2,
	input sel,
	output reg [15:0] mux2Out
	);
	
    always@(*) begin

	    if (sel) mux2Out = mux2In2;
	    else mux2Out = mux2In1;
	
    end

endmodule
