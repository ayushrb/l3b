// 5 input MUX
//Author: Niranjan

module mux_5In (
	input [15:0] in1,
	input [15:0] in2,
	input [15:0] in3,
	input [15:0] in4,
	input [15:0] in5,
	input [2:0] sel,
	output reg [15:0] out
	);
	
    always@(*) begin

	    case(sel)
		    3'b000: out=in1;
		    3'b001: out=in2;
		    3'b010: out=in3;
		    3'b011: out=in4;
		    3'b100: out=in5;
		    default: out=16'h0;
	    endcase
	
    end
endmodule
